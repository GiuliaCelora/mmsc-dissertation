"""
The function newton handles the solution of the non-linear system whether deflated or not.
The implementation is follow closely the one built for defcon python library. Further information can be found at:
	"https://bitbucket.org/pefarrell/defcon/src/master/"
"""
from dolfin import *
from petsc4py import PETSc
from numpy import isnan
import sys

class ConvergenceError(Exception):
    pass

class myNonLinearProblem(NonlinearProblem):
    """
    I define the nonlinearproblem as a subclass of the normal PETSc class
    NonlinearProblem in order to be able to impose the BC
    """

    def __init__(self, F, J, bcs=None):

        NonlinearProblem.__init__(self)
        self.residual=F
        self.Jacobian=J
        if bcs is not None: 
            self.bcs=bcs

    def F(self,b,x):
        assemble(self.residual, tensor=b)
        if hasattr(self,"bcs"): 
            for bc in self.bcs: bc.apply(b, x)

    def J(self, A, x):
        assemble(self.Jacobian, tensor=A)
        if hasattr(self,"bcs"): 
            for bc in self.bcs: bc.apply(A)


def getEdy(deflation, y, dy, vi_inact):
    deriv = as_backend_type(deflation.derivative(y)).vec()

    if vi_inact is not None:
        deriv_ = deriv.getSubVector(vi_inact)
    else:
        deriv_ = deriv

    out = -deriv_.dot(dy)

    if vi_inact is not None:
        deriv.restoreSubVector(vi_inact, deriv_)

    return out

    def setSnesMonitor(prefix):
        PETScOptions.set(prefix + "snes_monitor_cancel")
        PETScOptions.set(prefix + "snes_monitor")

    def setSnesBounds(snes, bounds):
        (lb, ub) = bounds
        snes.setVariableBounds(as_backend_type(lb.vector()).vec(), as_backend_type(ub.vector()).vec())



def compute_tau(deflation, state, update_p, vi_inact):
    if deflation is not None:
        Edy = getEdy(deflation, state, update_p, vi_inact)

        minv = 1.0 / deflation.evaluate(state)
        tau = (1 + minv*Edy/(1 - minv*Edy))
        return tau
    else:
        return 1

class DeflatedKSP(object):
    def __init__(self, deflation, y, ksp, snes):
        self.deflation = deflation
        self.y = y
        self.ksp = ksp
        self.snes = snes

    def solve(self, ksp, b, dy_pet):
        # Use the inner ksp to solve the original problem
        self.ksp.setOperators(*ksp.getOperators())
        self.ksp.solve(b, dy_pet)
        deflation = self.deflation

        if self.snes.getType().startswith("vi"):
            vi_inact = self.snes.getVIInactiveSet()
        else:
            vi_inact = None

        tau = compute_tau(deflation, self.y, dy_pet, vi_inact)

        dy_pet.scale(tau)

        ksp.setConvergedReason(self.ksp.getConvergedReason())

    def reset(self, ksp):
        self.ksp.reset()

    def view(self, ksp, viewer):
        self.ksp.view(viewer)

def newton(F, J, y, bcs, tolerance=None,deflation=None, num_iter=10):

    comm = y.function_space().mesh().mpi_comm()
    npproblem = myNonLinearProblem(F, J, bcs)

    solver = PETScSNESSolver()
    par=solver.parameters

    par["maximum_iterations"] = num_iter
 
    snes = solver.snes()
 
    solver.init(npproblem, y.vector())

    oldksp = snes.ksp
    defksp = DeflatedKSP(deflation, y, oldksp, snes)
    snes.ksp = PETSc.KSP().createPython(defksp, comm)
    snes.ksp.pc.setType('none')
    snes.setTolerances(rtol=0.0, atol = 1.0e-9,stol=0.0, max_it=100)
    snes.ksp.setTolerances(divtol=10000000000000000)

    try:
        # if I do not specify the arguments again it gives me error
        solver.solve(npproblem, y.vector()) 
    except ConvergenceError:
        pass
    except:
        import traceback
        traceback.print_exc()
        pass

    del defksp.snes # clean up circular references

    success = snes.getConvergedReason() > 0
    iters   = snes.getIterationNumber()
    return (success, iters)
