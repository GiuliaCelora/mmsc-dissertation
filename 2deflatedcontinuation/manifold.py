"""
This file contain the class DynamicMesh and Frontallist used to represent the manifold
"""
from dolfin import *
from numpy import pi
import numpy as np

class DynamicMesh(object):
    
    def __init__(self,bbox=None):
        self.node = [] #list of the vertex of the triangulation
        self.edge = [] # list with all the edges
        self.simplex = [] # list with the simpleces
        self.neighbour = [] # list of the collection of each simplex creating the neighbour of a points
        
        self.indexnode = -1
        self.indexed = -1
        self.indexsimplex= -1
        
        self.freeid = [] # when some node are cancelled we overwrite them without

        if bbox is not None: self.BBox = bbox
    
    def add_front_list(self,front):
        
        self.frontal = front

        
    def add_node(self, param, centre=None, task = "", flag=None):
        """
        param is the vector of the coordinate in the parameter space
        centre: points from which we have computed the new point (father and vertex of the simplex)
        
        returns the id of the node
        """
        
        if len(self.freeid)==0:
            self.indexnode += 1
            index=self.indexnode
            self.node.append([param,-1])
            self.neighbour.append([])
        else:
            index = self.freeid.pop()
            self.node[index] = [param,-1]
            self.neighbour[index] = []
        
        if centre is not None:
            if task == "merge":
		#merging operator
                flag=self.merge(index, centre)
            else:
                n = self.number_simplices(centre)
                v = [centre]
                edges=[]
                if isinstance(n,tuple):
		    # the neighbourhood has just an edge, this is used only for adding the third node in the triangulation
                    v = list(n[2])
                    edges.append(self.add_edge(index,v[0])) 
                    edges.append(self.add_edge(index,v[1])) 
                    # we just need to update the already know simplex
                    self.simplex[n[1]] += edges
                    self.neighbour[index] = [n[1]] 
			        # adding the simplex to the BoundedBox tree
                    self.BBox.add_box(n[1])
                    
                elif n==0:
                    edges.append(self.add_edge(index,centre)) 
                    self.add_simplex(edges[:])

                else:
                    v.append(self.extremenode(centre,1,"index")) # third vertex in the simplex

                    edges.append(self.find_edges_from_node(v[1],centre))
                    edges.append(self.add_edge(index,centre))
                    edges.append(self.add_edge(index,v[1])) 
                    self.add_simplex(edges[:])

                    
                    self.BBox.add_box(self.indexsimplex)
                
                if task=="close": # we complete the neighbourhood adding the missing edge
                    flag=self.close(centre,flag=flag)
                else:
                    flag=self.frontal.update(v,flag)
        return index, flag
    
    def close(self,centre, flag=None):
        """
        this routine closes the gap between the two edge points
        """
        vert = self.extremenode(centre,types="index")
        
        edges=[]
        edges.append(self.find_edges_from_node(vert[0],centre))
        edges.append(self.find_edges_from_node(vert[1],centre)) 
        edges.append(self.add_edge(vert[0],vert[1]))
        self.add_simplex(edges[:])
	
        self.BBox.add_box(self.indexsimplex)
        if hasattr(self,"frontal") & (flag is not None):
            return self.frontal.update(vert,flag)
        return None
     
            
    def add_edge(self,node1,node2):
        """
        the routine adds the edge connecting the two points to the list
        """
        newedge = [node1, node2]
        if len(self.freeid_edge)>0:
            indexid = self.freeid_edge.pop()
            self.edge[indexid]=newedge
        else: 
            self.indexed += 1
            indexid = self.indexed 
            self.edge.append(newedge)
        return indexid
        
        
    def add_simplex(self,edges):
        """
        the routine adds the simplex and updates the neighbour of all the verteces involved
        """
        self.indexsimplex += 1
        indexsimplex = self.indexsimplex 
        self.simplex.insert(indexsimplex,edges[:])
        setnode = self.find_node_from_simplex(indexsimplex)
        for node in setnode:
            self.update_neigh(node,indexsimplex,setnode-set([node])) 
    
    def update_neigh(self,centre,new_simplex,nodes):
        """
        The function adds a new simplex to the neighbour of the centre
        """
        neigh = self.neighbour[centre]
        
        if len(neigh)==0:
            neigh.append(new_simplex)
        else:
            if not isinstance(nodes, set):
                nodes = set(nodes)
                
            vertices = self.find_node_from_simplex(neigh[0])
            if len(nodes.intersection(vertices)) > 0:
                neigh.insert(0,new_simplex)
            else:
                neigh.append(new_simplex)
                    
    def find_edges_from_node(self,node1,node2):
        """
        find the index of the edges connecting the two nodes
        """

        idsimplex = list(set(self.neighbour[node1]).intersection(set(self.neighbour[node2])))
        idsimplex = idsimplex[0]
        
        for idedge in self.simplex[idsimplex]:
            if (node1 in self.edge[idedge]) & (node2 in self.edge[idedge]):
                return idedge

        return None
        
    def find_node_from_simplex(self,idsimplex):
        """
        receive as an input the index a simplex and return a set with the indeces of the nodes
        it cointains
        """
        
        edges = self.simplex[idsimplex]
        
        listnode=[]
        for edge in edges:
            listnode+= self.edge[edge]
            
        setnode = set(listnode)
        return setnode
    
    def complement(self,centre,extreme):
        """
	Input: 
	-centre: index of the centre of the neighbourhood
	-extreme: list with the extreme nodes of the neighbourhood

        Return a node in the neighbourhood of centre which is not an extreme point

        """
        B = set(extreme)
        simplex = self.neighbour[centre][0]
        A = self.find_node_from_simplex(simplex)
        return list(A-B)[0]
        
        
    def extremenode(self,centre,num=0,types=""):
        """
        Input:
	centre: index of the node at the centre of the neighbourhood

	num : integer that can be either 0 (both extreme node are returned),1(only the first extreme node is returned) 
	or 2 (only the second node is returned);

	type: represent the format in which the node are returned, if type = "index" the routine returns the indeces of 	the extreme nodes otherwise their coordinate in the paramenter space  
        """
        neigh = self.neighbour[centre]

        if len(neigh)==0 :
            return None
        if len(neigh)==1:
            node = list(self.find_node_from_simplex(neigh[0])-set([centre]))
            node = node[-1:]
        else:
            node=[]
            temp = list(self.find_node_from_simplex(neigh[0])-self.find_node_from_simplex(neigh[1]))
            node.append(temp[0])
            temp = list(self.find_node_from_simplex(neigh[-1])-self.find_node_from_simplex(neigh[-2]))
            node.append(temp[0])
        if num != 0:
            if types == "index":
                return node[edge-1]
            else:
                return self.node[node[edge-1]]
        else:
            if types == "index":
                return node
            else:
                return [self.node[node[0]][0],self.node[node[1]][0]]
    
    def number_simplices(self,centre):
        """
        returns the number of simpleces connected to centre
        """
        neigh = self.neighbour[centre]
        k = len(neigh)
        
        if k==1:
            vertices = self.find_node_from_simplex(neigh[0])
            if len(vertices) < 3: # the simplex is not complete
                return (0, neigh[0], list(vertices))
        return k
    
    def merge(self, newnode, centre):
        """
        this routine handles the merging operation
        """

        neigh = self.neighbour
        frontal = self.frontal

        nodes = self.extremenode(centre,types="index")
        
        old_edge = self.find_edges_from_node(centre, nodes[0])
        new_edge = self.find_edges_from_node(centre, nodes[1])

        if centre == self.extremenode(nodes[0],1,types="index"):
            neigh[newnode]=neigh[nodes[0]][::-1]
        else:
            neigh[newnode]=neigh[nodes[0]][:]
        
        if centre == self.extremenode(nodes[1],1,types="index"):
            neigh[newnode]+=neigh[nodes[1]]
        else:
            neigh[newnode]+=neigh[nodes[1]][::-1]

        check=[]
        for node in nodes:
            inf=frontal.delete(node)
            if inf!=None:
                check.append(node)
            for idsimplex in self.neighbour[node]:
                for idedge in self.simplex[idsimplex]:
                    if idedge == old_edge:
                        self.simplex[idsimplex].append(new_edge)
                        self.simplex[idsimplex].remove(old_edge)
                    elif node in self.edge[idedge]:
                        self.edge[idedge].append(newnode)
                        self.edge[idedge].remove(node)


                self.BBox.update_bounding_box(idsimplex,self["node",node,0])

            self.freeid.append(node)
        
        self.freeid_edge.append(old_edge)
        return check
        
    def __getitem__(self, x):

        if len(x)==2:
            return getattr(self, x[0])[x[1]]
        else:
            return getattr(self, x[0])[x[1]][x[2]]
    
    def update_simpleces(self,index):

        new_num_simplex = self.number_simplices(index)
        
        if isinstance(new_num_simplex,tuple):
            new_num_simplex = new_num_simplex[0]
            
        self.node[index][1] = new_num_simplex
    
    def functional_add(self,index,fun):
	"""
	the routine save the functional evaluated at point index
	"""
        
        node = self.node[index]
        node.append(fun)


    def create_mesh(self,mesh,func_index):
	"""
	this routine transform the dynamic mesh object into a FEniCS mesh
	"""

        editor=MeshEditor()
        editor.open(mesh,"triangle",2,3)
        n_ver=self.indexnode-len(self.freeid)+1
        editor.init_vertices(n_ver)
        editor.init_cells(self.indexsimplex+1)

        d_index=0
        for index in range(n_ver):
            idnode=index+d_index
            if idnode in self.freeid:
                d_index+=1
                idnode=index+d_index

            temp=list(self["node",idnode,0])
            temp.append(self["node",idnode,2][func_index])

            self.node[idnode].append(d_index)
            editor.add_vertex(index, np.array(temp))

        for index in range(self.indexsimplex+1):
            list_node=list(self.find_node_from_simplex(index))
            list_node = [node-self.node[node][-1] for node in list_node]
            editor.add_cell(index, np.array(list_node,dtype=np.uint))

        editor.close()





class FrontalList(object):
    
    def __init__(self, manifold):
        
        self.dic = {} # dictionary containig the nodes on the fronts. This is organized based on the number of simpleces 
	# incident at that point
        self.manifold = manifold
        self.manifold.add_front_list(self)
        self.priority = [] # a list containing the nodes with a gap angle smaller than pi/3, this 
    
    def get(self):
        """
        The routine select and cancel an element from the list 
        """
        
        dic = self.dic
        if len(dic) == 0:
            return None

        idvertex = None
        while len(self.priority)>0:
            idvertex = self.priority.pop()
            index = self.manifold["node",idvertex,1]
	    if index>=0:
		break
	    idvertex=None 

        if idvertex is None:
            index = max(dic.keys())
            try:
                idvertex = dic[index].keys()[0]
            except TypeError:
                idvertex = list(dic[index].keys())[0]

        (info, num_vertex) = self.delete(idvertex)


        return (self.manifold["node",idvertex,0],info,idvertex,num_vertex)
    
    def insert(self, index, num_simplex=-1, info=None):
        """
        This routine insert a a new element in the list 
	    index: ID of the node in the manifold.node list

	    num_simplex: number of simpleces incident in the node, if no value is passed the function evaluates it
	
	    info: a dictionary with the information of the node we want to keep track of

	    The function returns true if the neighbourhood contains more or equal to three simplices
        """
        
        if num_simplex==-1:
            self.manifold.update_simpleces(index)
            num_simplex = self.manifold["node", index, 1]
            
        
        if num_simplex not in self.dic.keys():
            self.dic[num_simplex] = {}
        
        if info is None:
            self.dic[num_simplex][index] = {}
        else:
            self.dic[num_simplex][index]= info.copy()
            
        return num_simplex > 3
    
    def update_info(self,index,**kwargs):
        """
        Add the additional information needed with the node
        """
        
        num_simplex = self.manifold["node", index, 1]
        info = self.dic[num_simplex][index]
        
        for key in kwargs.keys():
           info[key] = kwargs[key] 
           if key == "gap_angle":
               if (info[key][0]<pi/3) & (index not in self.priority):
                   self.priority.append(index)
        
    
    def update(self,nodes,flag=None):
        """
        This routine update the position of the nodes in the list.
	The flag list collects the index of the node whose gap angle needs to be evaluated
        """

        dic = self.dic
        angle_fix = []
        
        for node in nodes:
            old_num_simplex = self.manifold["node", node, 1]
            
            if old_num_simplex != -1:
                info = dic[old_num_simplex].pop(node, None)
                if len(dic[old_num_simplex])==0:
                    dic.pop(old_num_simplex,None)
                if self.insert(node,info=info):
                    angle_fix.append(node)
        if flag is None:            
            return angle_fix
        else:
            return flag.union(angle_fix)
            

    
    def delete(self, index):
	"""
	This routine receive the index of a node and deletes it from the front
	"""
        
        num_simplex = self.manifold["node",index,1]

        if num_simplex!=-1: 
            try:
                self.priority.remove(index)
            except ValueError:
                pass
            self.manifold.node[index][1]=-1
            info=self.dic[num_simplex].pop(index, None)
            if len(self.dic[num_simplex])==0:
                self.dic.pop(num_simplex)
            return (info,num_simplex)
        
        
            
