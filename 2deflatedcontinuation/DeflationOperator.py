"""
The class of ShiftedDeflation here follows closely the one implented by Farrell et al. for defcon package. 
	"https://bitbucket.org/pefarrell/defcon/src/master/"
The ShiftedDeflation exploits the Shifted deflation operator, see Definition 3.1.2 at page 36 of the thesis.
"""

from dolfin import * 
from ufl import product



class DeflationOperator(object):

    def set_parameters(self, params):
        self.parameters = params

    def deflate(self, roots):
        self.roots.append(roots)

    def evaluate(self):
        raise NotImplementedError

    def derivative(self):
        raise NotImplementedError


class ShiftedDeflation(DeflationOperator):
    
    def __init__(self, problem, power, shift,param):
        self.problem = problem
        self.power = power
        self.shift = shift
        self.roots = []
	self.param=param

    def normsq(self,y,root):
	if self.problem.norm=="H1": 
		const = sqrt(self.param[0])
		return inner(y-root,y-root)*dx+const*inner(grad(y-root),grad(y-root))*dx
	else:
		return inner(y-root,y-root)*dx
        

    def clear(self):

        for sol in self.roots:
            del sol
        self.roots=[]

    def evaluate(self, y):
        m = 1.0
        
        for root in self.roots:
            normsq = assemble(self.normsq(y, root))
            factor = normsq**(-self.power/2.0) + self.shift
            m *= factor

        return m

    def derivative(self, y,par=0):
        if len(self.roots) == 0:
            deta = Function(y.function_space()).vector()
            return deta

        p = self.power
        factors  = []
        dfactors = []
        dnormsqs = []
        normsqs  = []
        
        

        for root in self.roots:
            form = self.normsq(y, root)
            normsqs.append(assemble(form))
            dnormsqs.append(assemble(derivative(form, y)))

        for normsq in normsqs:
            factor = normsq**(-p/2.0) + self.shift
            dfactor = (-p/2.0) * normsq**((-p/2.0) - 1.0)

            factors.append(factor)
            dfactors.append(dfactor)

        eta = product(factors)

        deta = Function(y.function_space()).vector()

        for (solution, factor, dfactor, dnormsq) in zip(self.roots, factors, dfactors, dnormsqs):
            deta.axpy(float((eta/factor)*dfactor), dnormsq)

        return deta
