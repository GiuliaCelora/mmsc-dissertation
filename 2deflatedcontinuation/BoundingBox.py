"""
This class is used to represent the manifold as a tree of BoundingBox, each representing a simplex in the triangulation.
This is used in the 2d deflated continuation as described In Section 3.2 

Notice that this rely on the Index object provided by the rtree python package. For more details see:
"http://toblerity.org/rtree/"
"""
 
from dolfin import *
from rtree import index
import numpy as np
import scipy.linalg as la
from multi_problem import fetch_R



class BBox(object):
# the class contains the bounding box with the different neighbours and the MultiProblem
	def __init__(self, problem,branch):

		self.box = index.Index()
		self.problem = problem
		self.branch=branch


	def set_manifold(self,manifold):
		self.manifold=manifold

	def add_box(self,index):

		manifold=self.manifold
	
		nodes=[manifold["node",node,0] for node in manifold.find_node_from_simplex(index)]
		
		x=[node[0] for node in nodes]
		y=[node[1] for node in nodes]
		self.box.insert(index,(min(x),min(y),max(x),max(y)))

	def fetch_solution(self,point):
		"""
		Given a point in the paramenter space (lambda*) it returns the corresponding solutions u*
		"""
		BBox=self.box


		simpleces = set(BBox.intersection((point[0],point[1],point[0],point[1])))

		# if the index belong to the manifold we do not need to consider it


		sol=[]
		while len(simpleces)>0:
			k = simpleces.pop()
			(x,idverteces) = self.is_inside(point,k)
			if x is not None:
				delete=[]
				for idvertex in idverteces:
					delete+=list(self.manifold.neighbour[idvertex])
				simpleces-=set(delete)
				sol.append((x,idverteces))

		return sol

	def is_inside(self, point, simplex):
		"""
		Using baricentric coordinates it computes if a point in the parameter space (point) is inside the 			projected simplex or not
		"""
		manifold=self.manifold

		idverteces = list(manifold.find_node_from_simplex(simplex))
		v1,v2,v3 = [manifold.node[vertex][0] for vertex in idverteces]
		A=np.array([[v1[0],v2[0],v3[0]],[v1[1],v2[1],v3[1]],[1,1,1]])
		b=np.array([point[0],point[1],1])
		x=la.solve(A,b)
		inside = (x<0).sum()
		if not inside:
			return (x,idverteces)
		else:
			return (None,None)


	def finding_root(self,point):
		"""
		This function returns all the solution correspoding to the parameter specified
		in point belonging to the specific branch of the manifold specified
		"""

		prefix = self.problem.file + "/db %d" % self.branch

		sols = self.fetch_solution(point=point)
		function_space= self.problem.V
		roots=[]

		if len(sols)==0:
			return roots

		for (x,ids) in sols:

			new = Function(function_space, prefix + "/point-%d-state.xml" % ids[0])
			old1 = Function(function_space, prefix + "/point-%d-state.xml" % ids[1])
			old2 = Function(function_space, prefix + "/point-%d-state.xml" % ids[2])
			new.assign(float(x[0])*new+float(x[1])*old1+float(x[2])*old2)
			success=self.problem.solve_residual(point,new) 
			if not success:
				del old1
				del old2
				del new
				pass
			else:
				roots.append(new.copy(deepcopy=True))
				del old1
				del old2
				del new

		return roots

	def control_intersection(self,state):
		"""
		The function return True if the point overlaps with the part of the manifold already computed
		False otherwise
		"""

		print("Looking for a superposition with  %d"%self.branch)
		problem=self.problem
		param = fetch_R(problem.ac_to_parameter(state))
		z = problem.ac_to_state(state)
		solutions = self.finding_root(point=param)

		for sol in solutions:

			difference = assemble(inner(z-sol,z-sol)*dx)
			if self.problem.norm=="H1":
				difference += sqrt(param[0])*assemble(inner(grad(z-sol),grad(z-sol))*dx)
			print("Norm of the difference=%.8f"%difference)
			if sqrt(difference) < 1E-8:
				print("Overlap")
				del z 
				for x in solutions: del x
				return True

		print("No Overlap detected")
		return False

	def update_bounding_box(self,index,centre):
		"""
		When a simplex is updated this function is called to update the 
		corresponding bounding box
		"""
		
		box=(centre[0]-self.problem.step_max,centre[1]-self.problem.step_max,centre[0]+self.problem.step_max,centre[1]+self.problem.step_max)
		self.box.delete(index, box)
		

		self.add_box(index)
				
