 
from dolfin import *
from mshr import *
from numpy import pi
from multi_problem import MultiProblem
from 2_deflated_continuation import Worker

interval_mesh=IntervalMesh(1000,-1,1)


def residual(u, par, v):
       	(par1,par2)=split(par)
        x = SpatialCoordinate(interval_mesh)[0]

        F = (- par1**2*inner(grad(u), grad(v))*dx+ par2*(1-x*x) * inner(u, v)*dx+ inner(u*u, v)*dx- inner(Constant(1), v)*dx)

        return F


def BCs(V):
	return [DirichletBC(V, 0.0, "on_boundary")]

def fun(u):
	V = u.function_space()
        j = assemble(u*u*dx)
        g = project(grad(u)[0], V)((-1.0,))
        return j*g/100
	

V=FunctionSpace(interval_mesh, "CG", 1)
u_0=Function(V)
tau_min = 1.2
gamma_min = pi/6.
step_min = 0.005
step_max = 0.08
bound = [(0.08,-3.5),(0.5,4.0)]
bcs=[DirichletBC(V, 0.0, "on_boundary")]

lam_0 = [(0.25,2.0)]


problem = MultiProblem(interval_mesh,residual,V,tau_min,gamma_min,step_min,step_max, bound, [fun],FunctionalBound=[(-2.1,2.1)],bcs=BCs,norm="H1")
main = Worker([2,1],[([0.25,0.08],[3.0,3.0],25),([0.2,0.5],[3.0,3.0],25)],problem)
main.run(problem,"examples/Carrier",lam_0,max_iter=4)
