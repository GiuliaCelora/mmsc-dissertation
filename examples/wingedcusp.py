
from dolfin import *
from mshr import *
from numpy import pi
from arc import arclength
from multi_problem import MultiProblem
from 2_deflated_continuation import Wworker

def F(u,par,w):
    par1,par2 = split(par)
    form =inner(u**3-2*par1*u+par1**2+par2*par1+1,w)*dx
    return form
    
def functional(z):
    return z((0.5,))
def BCs(V):
	return None


mesh = UnitIntervalMesh(2)
V = FunctionSpace(mesh, "R", 0)
tau_min = 1.2
gamma_min = pi/6.
step_min = 0.005
step_max = 0.4
bound = [(-5,-10),(5,10)]

lam_0 = [(-1.0,2.0)] 
u_0 = Function(V) 
u_0.interpolate(Constant(0.0))

problem = MultiProblem(mesh,F,V,tau_min,gamma_min,step_min,step_max,bound,[functional],bcs=BCs)
main = Worker([2,1],[([-5,-5],[-5,5],20),([5,5],(-5,5),20)],problem)

main.run(problem,"examples/ODE",lam_0,[u_0],max_iter=3)
