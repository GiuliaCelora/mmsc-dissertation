 
from dolfin import *
from mshr import *
from numpy import pi
from multi_problem import MultiProblem
from main_body import worker

interval_mesh=IntervalMesh(600,-1,1)
#test1 F = (- par1**2*inner(grad(u), grad(v))*dx+ 2*(1-x*x) * inner(u, v)*dx+ inner(u*u, v)*dx- inner(par2, v)*dx) par2 in [0.01,4.0]
#F = (- par1**2*inner(grad(u), grad(v))*dx+ par2*(1-x*x) * inner(u, v)*dx+ inner(u*u, v)*dx- inner(Constant(1), v)*dx)


def residual(u, par, v):
       	(par1,par2)=split(par)
        x = SpatialCoordinate(interval_mesh)[0]

        F = (- par1**2*inner(grad(u), grad(v))*dx+ 2*(1-x*x) * inner(u, v)*dx+ inner(u*u, v)*dx- inner(par2, v)*dx)

        return F


def BCs(V):
	return [DirichletBC(V, 0.0, "on_boundary")]

def fun(u):
	V = u.function_space()
        j = assemble(u*u*dx)
        g = project(grad(u)[0], V)((-1.0,))
        return j*g/100

def fun2(u):
        return assemble(inner(grad(u),grad(u))*dx)

	
V=FunctionSpace(interval_mesh, "CG", 1)
u_0=Function(V)
tau_min = 1.5
gamma_min = pi/6.
step_min = 0.001
step_max = 0.05
#bound = [(sqrt(0.01),-4.0),(0.5,8.0)]
bound = [(0.095,-0.09),(0.6,3.6)]
bcs=[DirichletBC(V, 0.0, "on_boundary")]

lam_0 = [(0.25,1.0)]


problem = MultiProblem(interval_mesh,residual,V,tau_min,gamma_min,step_min,step_max, bound, [fun,fun2],FunctionalBound=[(-2.1,2.1)],bcs=BCs,norm="H1")
#arclength(problem,lam_0, u_0=None, file= manifold)
main = worker([2,1],[([0.25,0.1],[2.0,2.0],15),([0.25,0.6],[2.0,2.0],10)],problem)
#main = worker([2,1],[([0.12,0.25],[2.0,2.0],12)],problem)
main.run(problem,"examples/Carrier_wrong",lam_0,max_iter=3)
