from dolfin import *
from mshr import *
from numpy import pi
from arc import arclength
from multi_problem import MultiProblem
from main_body import worker


interval_mesh=IntervalMesh(400, 0, 1)


def residual(u, par, v):

	(par1,par2)=split(par)
        F = (- inner(grad(u), grad(v))*dx+ par1* exp(par2*u)*v*dx)

        return F


def BCs(V):
	return [DirichletBC(V, 0.0, "on_boundary")]

def fun(u):
 	j = u((0.5,))
        return j
	

V=FunctionSpace(interval_mesh, "CG", 1)
u_0=Function(V)
tau_min = 1.2
gamma_min = pi/6.
step_min = 0.005
step_max = 0.08
bound = [(-4.0,0.01),(4.0,4.0)]

lam_0 = [(0.0,1.0)]


problem = MultiProblem(interval_mesh,residual,V,tau_min,gamma_min,step_min,step_max, bound, [fun],FunctionalBound=[(-1.0,10.0)],bcs=BCs)
main = worker([2,1],[([-4.0,4.0],[0.01,4.0],15)],problem)
main.run(problem,"examples/Bratu",lam_0,max_iter=3)
