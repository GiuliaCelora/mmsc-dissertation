import sys  
sys.path.insert(0,'/home/fenics/shared/')  
from dolfin import *
from mshr import *
from numpy import pi
from arc import arclength
from multi_problem import MultiProblem
from main_body import worker

Lx=1./18.;
LR=2.;
Lg=.2;
LR=.2*LR;
Lg=.2*Lg;
Lx=.1*Lx;
f1r=0.00117;
f1g=-0.0137;
f1rr=-0.00000854;
f1rg=-0.000407;
f1gg=0.00212;
a1=3.67;
b10=-0.0975;
b1r=-0.00392;
b1g=0.0543;
f2r=0.000681;
f2g=0.00955;
f2rr=-0.00000521;
f2rg=-0.000216;
f2gg=-0.00985;
a2=1.19;
b20=0.0331;
b2r=0.000476;
b2g=-0.00546;

f1r =LR*f1r/Lx**2;
f1g =Lg*f1g /Lx**2;
f1rr=LR**2*f1rr/Lx**2;
f1rg=LR*Lg*f1rg/Lx**2;
f1gg=Lg**2*f1gg/Lx**2;

b1r =LR*b1r/Lx;
b1g =Lg*b1g/Lx;

f2r =LR*f2r/Lx**2;
f2g =Lg*f2g/Lx**2;
f2rr=LR*LR*f2rr/Lx**2;
f2rg=LR*Lg*f2rg/Lx**2;
f2gg=Lg*Lg*f2gg/Lx**2;

b2r =LR*b2r/Lx;
b2g =Lg*b2g/Lx;

#def residual(u, par, v):
#        par1,par2 = split(par)
#        (x,y) = split(u)
#        (v_x,v_y) = split(v)
#        form = (x*(x**2 +a1*y**2 - (f1r*par1 + f1g*par2 + f1rr*par1**2 + f1rg*par1*par2 + f1gg*par2**2) + (b10 + b1r*par1 + b1g*par2)*y)*v_x*dx
#                 + (y*(a2*x**2 + y**2 - (f2r*par1 + f2g*par2 + f2rr*par1**2 + f2rg*par1*par2 + f2gg*par2**2)) + (b20+ b2r*par1 + b2g*par2)*x**2)*v_y *dx)

#        return form
a = 0.03 
b = 2.3
e = 0.26
c = -0.37
d = 2.4
f = 28.8
def residual(u,par,v):
        par1,par2 = split(par)
        (x,y) = split(u)
        (v_x,v_y) = split(v) 
        form = ((x*y**2 + 2*x**3 + x*y - x*par1 + x*par2)*v_x*dx + 
                (x**2*y + 2*y**3 - y*par1 - y*par2 - x**2)*v_y*dx)
        return form

def functional(u):
	(x,y) = split(u)
	return x((0.5,))+y((0.5,))

def fun_x(u):
	(x,y) = split(u)
	return x((0.5,))

def fun_y(u):
	(x,y) = split(u)
	return y((0.5,))	

def BCs(V):
	return None


mesh = UnitIntervalMesh(2)
V_element=VectorElement("R",mesh.ufl_cell(),0,dim =2)
V = FunctionSpace(mesh, V_element)
tau_min = 1.2
gamma_min = pi/6.
step_min = 0.02
step_max = 0.05
bound = [(-4.0,-2.0),(4.0,3.0)]


lam_0 = [(3.0,1.0)]
#y_0 = 0.5
u_0 = Function(V)
u_0.interpolate(Constant((1.0,-1.0)))



problem = MultiProblem(mesh,residual,V,tau_min,gamma_min,step_min,step_max, bound, [functional,fun_x,fun_y],bcs=BCs)
#arclength(problem,lam_0, u_0=None, file= manifold)
main = worker([2,1],[(0.0,0.5),(0.0,-0.5),(-0.5,0.0),(0.5,0.0)],problem)
main.run(problem,"TCflow/TCflow1",lam_0,[u_0])
