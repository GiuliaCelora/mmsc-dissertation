
from dolfin import *
from mshr import *
from numpy import pi
from arc import arclength
from multi_problem import MultiProblem
from 2_deflated_continuation import Worker

def residual(theta, par, v):
        par1,par2 = split(par)

        form = inner(grad(theta), grad(v))*dx-par1**2*sin(theta)*v*dx+par2*cos(theta)*v*dx

        return form

def fun_1(theta):
	g = Expression('x[0]', element = V.ufl_element())
	j = sqrt(assemble(inner(theta,theta)*dx))
#return j
	t = project(grad(theta)[0], V)
	return assemble(inner(g, theta)*dx)

def fun_2(theta):
	j = sqrt(assemble(inner(theta,theta)*dx))
	t = project(grad(theta)[0], V)
	return j*t((0.0,))

def fun_3(theta):
	return assemble(inner(grad(theta), grad(theta))*dx)

def BCs(V):
	return [DirichletBC(V, 0.0, "on_boundary")]

mesh=UnitIntervalMesh(500)
V=FunctionSpace(mesh, "CG", 1)
tau_min = 2.
gamma_min = pi/6.
step_min = 0.01
step_max = 0.2
bound = [(0.0,-5.0),(4*pi,5.0)]
bcs=[DirichletBC(V, 0.0, "on_boundary")]

lam_0 = [(2.0,0.5)]



problem = MultiProblem(mesh,residual,V,tau_min,gamma_min,step_min,step_max, bound, [fun_1,fun_2,fun_3], FunctionalBound=[(-10,10),(-60,60)],bcs=BCs)
#arclength(problem,lam_0, u_0=None, file= manifold)
main = Worker([2,1],[([2.0,0.0],[0.5,0.5],5),([2.0,4*pi],[0.5,0.5],20)],problem)
main.run(problem,"examples/elastica",lam_0,None)



