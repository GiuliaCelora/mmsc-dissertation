import sys  
sys.path.insert(0,'/home/fenics/shared/')  
from dolfin import *
from mshr import *
from numpy import pi
from arc import arclength
from multi_problem import MultiProblem
from main_body import worker



def residual(u, par, v):
        par1,par2 = split(par)
	(x,y)=split(u)
	(v_x,v_y)=split(v)
        form = (x*(x*x-y*y-par1)-2*x*y*y-par2)*v_x*dx+(2*x*x*y+y*(x*x-2*y*y-par1))*v_y*dx

        return form

def functional(u):
	(x,y)=split(u)
	return x((0.5,))+y((0.5,))


def BCs(V):
	return None


mesh = UnitIntervalMesh(2)
V_element=VectorElement("R",mesh.ufl_cell(),0,dim =2)
V = FunctionSpace(mesh, V_element)
tau_min = 2.
gamma_min = pi/6.
step_min = 0.005
step_max = 0.1
bound = [(-5.0,-5.0),(5.0,5.0)]


lam_0 = [(-0.3,0.0)]
u_0=Function(V)
u_0.interpolate(Constant((0.0,-0.4**3)))

problem = MultiProblem(mesh,residual,V,tau_min,gamma_min,step_min,step_max, bound, [functional],bcs=BCs)
#arclength(problem,lam_0, u_0=None, file= manifold)
main = worker([2,1],[([-2.5,-2.5],[-5.0,5.0],20),([2.5,2.5],[-5.0,5.0],20)],problem)
main.run(problem,"examples/cusp",lam_0,[u_0],max_iter=3)
