The code has been implemented in relation to the project:

"Bifurcation Analysis with Two Parameters"

by the candidate Giulia Laura Celora under the supervision of Dr. Patrick Farrell.

In the folder "2deflatedcontinuation" the different function and object defined for the implementation of our 
2D deflated continuation algorithm. 

In the "example" folder, there are instead the script releted to examples presented in the aforementioned work:

- Winged cusp

- Elastica

- Carrier's problem

A full description of the algorithm can be found in the pdf format of the thesis here upload. 

For any further information, contact: Giulia.Celora@maths.ox.ac.uk