from arc import arclength
from BoundingBox import BBox
from multi_problem import fetch_R
from newton import newton
from dolfin import *
import os
import numpy as np
from DeflationOperator import ShiftedDeflation
import time
import matplotlib.pyplot as plt

class worker(object):

    def __init__(self, deflation,intervals,problem):
	"""
	Input:
	- deflation: a list containing the shift and the power for the shifted operator
	- intervals: a list of the intervals where to apply the deflation step, each interval is defined by an initial 		point, a final point and the number of steps to compute in between
	- problem: represents the multiparameter problem 
	"""
        self.branches = -1 #saves the number of the branches found
        self.deflation = deflation
        self.problem = problem
        self.bbox = []
        self.manifold = []
        self.deflation_lines = intervals

    def add_BBox(self,bbox):
        self.bbox.append(bbox)

    def add_deflation_mesh(self):
	self.deflation_mesh = []
	for (dlambda,dgamma,num_point) in self.deflation_lines:
	     lmbda=list(np.linspace(dlambda[0],dlambda[1],num_point+1))
	     gamma = list(np.linspace(dgamma[0],dgamma[1],num_point+1))
	     self.deflation_mesh.append([[(a,b),0] for (a,b) in zip(lmbda,gamma)])
	

    def add_manifold(self, manifold):
        """
        a list of the manifold related to the different bounding box
        """
	self.manifold.append(manifold)
        self.bbox[-1].set_manifold(manifold)
	
    def add_solution_branch(self):
	""" 
	This routine keeps track of the solution known at each point in the deflation mesh
	"""

	for index in range(len(self.deflation_mesh)):
		index_2=0
		for (lam,num_sol) in self.deflation_mesh[index]:
			new_solution = self.find_solution(lam)
			
			for sol in new_solution:
				File(self.problem.file+"/deflation/%d" % index + "/%d"%index_2 +"-sol%d.xml"%num_sol) << sol
				num_sol+=1
			self.deflation_mesh[index][index_2][1] = num_sol
			index_2+=1

    def upload_old_solution(self, index_plane,index_step):
	
	new_sol=[]
	for i in range(self.deflation_mesh[index_plane][index_step][1]):
		sol = Function(self.problem.V, self.problem.file+"/deflation/%d" % index_plane + "/%d"%index_step +"-sol%d.xml"%i)
		new_sol.append(sol.copy(deepcopy=True))
	return new_sol



    def overlap(self,state):
        """
        This function return true the new node overlaps with a part of the manifold already computed
        """
        box = self.bbox[-1]
       	return box.control_intersection(state)

        

    def find_solution(self,param):
	"""
	This routine find the solution belonging to the branch corresponding to the value of the parameter specified
	"""
        
        return self.bbox[-1].finding_root(point=param)

    def deflate(self,param_,deflation=None,initial_guess=None,indeces=None):
        """
        Function that manages the deflation procedure
        """
        if self.deflation is None:
        	return
        	
        problem=self.problem


        sol = Function(problem.V)

        param = Function(problem.R)
        param.assign(Constant(param_))
        success = True
	
        F = problem.residual(sol,param,TestFunction(problem.V))
        J = derivative(F,sol,TrialFunction(problem.V))

        if initial_guess is not None: 
            sol.assign(initial_guess)

        if deflation is None: # definition of the operator
            deflation = ShiftedDeflation(self.problem,self.deflation[0],self.deflation[1],param_)
            deflation.roots += self.upload_old_solution(indeces[0],indeces[1])
	    deflation.param=param_
	
        try:
            success,iteration = newton(F, J, sol, problem.bcs(problem.V) , deflation=deflation,num_iter=100)
	    deflation.roots.append(sol.copy(deepcopy=True))
        except:
            success = False
            iteration = 0 

        if 0<=iteration<= 2:
	    deflation.roots.append(sol.copy(deepcopy=True))
            del sol
            sol = None
            success = False

        return success,sol,deflation
        


    def run(self, problem, prefix, lam0, u0=None, max_iter=2):

        try:
            lam_0 = lam0.pop(0)
        except IndexError:
            return

        if u0 is not None:
            u_0 = u0.pop(0)
        else:
            u_0 = None

        # Solving for the given initial guess
        start_time = time.time()
        self.branches+=1
	self.add_deflation_mesh()
        self.add_BBox(BBox(problem,self.branches))
        problem.BBox = self.bbox[-1]
        arclength(problem, self, max_iter, [lam_0], [u_0], file= prefix )
        
	print("branch %d"%self.branches+"--- %s seconds ---" % (time.time() - start_time))
        
        self.add_solution_branch()
	del self.bbox[-1]
	del self.manifold[-1]
        
	# Deflation step as described in Chapter 3 of the thesis

        for plane_index in range(len(self.deflation_mesh)):
           index_step=1

           for (point,num_sol) in self.deflation_mesh[plane_index][:-1]:
		lmbda=point[0]
		gamma=point[1]
                newlam=self.deflation_mesh[plane_index][index_step][0]
            # Once computed the branch we use deflation to find new branches
            
		prev=self.upload_old_solution(plane_index,index_step-1)
		print(len(prev),"point_prev:",(lmbda,gamma),"index",index_step)
		deflation=None

                for sol_prev in prev:
                    
                    success,sol,deflation = self.deflate(newlam,deflation=deflation,initial_guess=sol_prev,indeces=[plane_index,index_step])

                    while success:

                        self.branches += 1
                        print("computing branch%d"%self.branches)
                        self.add_BBox(BBox(problem,self.branches))
			plt.figure()
			plt.plot(sol.vector().get_local(),'b-')
			plt.title("Solution at point (%.6f,%.6f)"%newlam)
			plt.savefig("solution_branch%d.pdf"%self.branches)
			arclength(problem, self, max_iter,[newlam], [sol], file= prefix,overlap=False)
			self.add_solution_branch()
			deflation.roots += self.find_solution(newlam)
                        print("branch %d"%self.branches+"--- %s seconds ---" % (time.time() - start_time))
			try:
				del self.bbox[-1]
				del self.manifold[-1]
			except:
				pass
                        success,sol,deflation= self.deflate(newlam,deflation=deflation,initial_guess=sol_prev)
		try:
                    del prev
		    del deflation
		except:
		    pass

                index_step+=1

        try:
	    del prev
	except:
	    pass

        

 # we now continue with the next initial condition
        self.run(problem, prefix, lam0, u0)






