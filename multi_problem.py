# -*- coding: utf-8 -*-
"""
Created on Fri Jul 13 00:39:10 2018

@author: celora
"""

import pdb
from dolfin import *
from newton import newton
import numpy as np
import os

def fetch_R(r):
    rval = r.vector().get_local()
    if len(rval) == 0:
        rval = [0.0,0.0]
    else:
        rval = rval[0:2]

    return rval


class MultiProblem(object):
    
    def __init__(self, mesh, F, V, tau, gamma_min,step_min, step_max,par_bound, functional, FunctionalBound=None, bcs=None, norm = "L2"):
        
        self.residual=F
        self.V = V
        self.mesh = mesh
        self.tau = tau # adaptive parameter
        self.gamma_min = gamma_min 
        self.step_min = step_min
        self.step_max = step_max
        self.bound = par_bound
        self.bcs = bcs
        self.functional = functional
        self.func_bound = FunctionalBound
        self.prefix= ""
        self.file = ""
	self.norm = norm
	
    
    def add_initial_condition(self,param_0,state_0,frontal,worker):
        """
        add an initial point to the manifold 
        need to add the case in which just the initial parameters are given
        """
        
        for par,state in zip(param_0,state_0):

            if state is None:
		state = Function(self.V)

        	param = Function(self.R)
        	param.assign(Constant(par))
        	success = True
	
        	F = self.residual(state,param,TestFunction(self.V))
        	J = derivative(F,state,TrialFunction(self.V))
                success,iteration = newton(F, J, state, self.bcs(self.V),num_iter=20)
                if not success:    
                    return False

            index, check=self.manifold.add_node(par)
            File(self.prefix + "/point-%d-state.xml" % index) << state

            if not self.InOmega(index):
                os.remove(self.prefix + "/point-%d-state.xml" % index)
                return False

	    frontal.insert(index)
            fun=[fun(state) for fun in self.functional]
            self.manifold.functional_add(index,fun)      

            return True 
            
    def InOmega(self, index=None ,point=None):
        """
        The function return True if the point belongs to the subset of the parameter
        space we are interested in; this in defined by two points in R^2:
        - (lam_min,gamma_min) and (lam_max,gamma_max)
        """

        if index is not None:
            param = self.manifold["node",index,0]
            if self.func_bound is not None:
                try:
                    funcs = self.manifold["node",index,2]
                    if isinstance(funcs,float):
                        if (self.func_bound[0][0] > funcs) or (self.func_bound[0][1]< funcs):
                                return False
                    else:
                        for j in range(len(funcs)):
                            if self.func_bound[j] is not None:
                                if (self.func_bound[j][0] > funcs[j]) or (self.func_bound[j][1]< funcs[j]):
                                    return False
                except IndexError:
                    pass
        else:
            param = point

        y_min = self.bound[0]
        y_max = self.bound[1]
        
        if (y_min[0]<= param[0]<= y_max[0]) & (y_min[1]<= param[1]<= y_max[1]):
            return True
            
        return False
        
    def SetManifold(self,manifold):
        
        self.manifold = manifold
    
    def SetupSpaces(self):
        """
        this function returns the space of the solution and of the parameters; it
        also build and return the mixed space used in the continuation
        """

        self.state_residual = None
        self.state_residual_derivative = []

        mesh = self.mesh
        state_element = self.V.ufl_element()

        R_element=VectorElement("R", self.V.ufl_cell(), 0, dim=2)
        self.R =FunctionSpace(mesh, R_element)

        ac_element = MixedElement([state_element, R_element])
        self.ac_space = FunctionSpace(mesh, ac_element)

        return (self.V, self.R, self.ac_space)

    def solve_residual(self,param,guess):
        function_space=self.V

        if self.bcs is not None:
            bcs=self.bcs(function_space)
        else:
            bcs=None

        v = TestFunction(function_space)

        par_0=Function(self.R)

        par_0.interpolate(Constant((param[0],param[1])))

        success=True

        F = self.residual(guess,par_0,v)
        J = derivative(F,guess,TrialFunction(function_space))
        
        (success,iteration)=newton(F,J,guess,bcs)
        return success

    def ac_to_state(self, ac, deep=True):
        # Given the mixed space representing what we're solving the augmented
        # arclength system for, return the part of it that denotes the state
        # we're solving for
        if deep:
            return ac.split(deepcopy=True)[0]
        else:
            return split(ac)[0]

    def ac_to_parameter(self, ac, deep=True):
        # Given the mixed space representing what we're solving the augmented
        # arclength system for, return the part of it that denotes the parameter
        # we're solving for
        if deep:
            return ac.split(deepcopy=True)[1]
        else:
            return split(ac)[1]


    def ac_residual(self, ac, ac_prev, step, direction, test, tangent):
        """
        Returns the augmented system to computing the corrective step in the 
        archlength
        """ 

        (z, lmbda)  = split(ac)
        (w,  mu)    = split(test)
        (mu0,mu1) = split(mu)

        if self.state_residual is None:
            self.state_residual = self.residual(z, lmbda, w) 


        ac_residual = ( self.state_residual
                       + mu0*(inner(tangent[0],ac-ac_prev) - step*direction[0])*dx
                       + mu1*(inner(tangent[1],ac-ac_prev) - step*direction[1])*dx )

        return ac_residual

    def base_tangent_plane(self, ac, tangent, test, trial, tangent_prev, hbcs):
        #Compute the a basis for the tangent plane

        (z, mu)  = split(test)
        (mu1,mu2) = split(mu)

        # we create and solve the system for the first vector in the basis
        F = self.tangent_residual(ac, tangent[0], tangent_prev, z, mu1, mu2,0)
        J = self.tangent_jacobian(F, tangent[0], trial)
        (success,iteration) = newton(F, J, tangent[0], hbcs)


        if success:
            print("Find first vector tangent base %d iteration"%iteration)
        # we create and solve the system for the second vector in the basis
            F = self.tangent_residual(ac, tangent[1], tangent_prev, z, mu2, mu1,1)
            J = self.tangent_jacobian(F, tangent[1], trial)
        
            (success,iteration) = newton(F, J, tangent[1], hbcs)  
            
            if success:
                print("Find second vector tangent base %d iteration"%iteration)
                self.orthonormal(tangent[0],tangent[1])  
            else:
            	return False
        # given the two vector we apply Gram-Smith to create an orthonormal basis
    	else:
    		return False
      
        return success

    def ac_jacobian(self, ac_residual, ac, trial):
        return derivative(ac_residual, ac, trial)

    def tangent_residual(self, ac, tangent, tangent_prev, z,mu1,mu2,index=0):
        """
        Computes the augmented system to solve to compute the basis function.
        In the case in which no information of the tangent at other point is given,
        it just computes the tangent vectors in the main direction
        """

        if len(self.state_residual_derivative)<index+1:
            
            self.state_residual_derivative.append(derivative(self.state_residual, ac, tangent)) 


        lmbd = self.ac_to_parameter(tangent, deep=False)


        if tangent_prev is not None:
            phi1_prev = tangent_prev[index]
            phi2_prev = tangent_prev[index-1]

            normalisation_condition1 = inner(tangent,phi1_prev)- Constant(1)
            normalisation_condition2 = inner(tangent,phi2_prev)
            
            tangent_residual = (self.state_residual_derivative[index] 
                                + mu1*normalisation_condition1*dx 
                                + mu2*normalisation_condition2*dx)    
        
        else:
            
            normalisation_condition =  lmbd[index] - Constant(1)*self.step_min
            
            tangent_residual = (self.state_residual_derivative[index] 
                                + mu1*normalisation_condition*dx  
                                + mu2*lmbd[index-1]*dx)
        return tangent_residual

    def tangent_jacobian(self, tangent_residual, tangent, trial):
        return derivative(tangent_residual, tangent, trial) 

    def boundary_conditions(self, V):
        """
        the function returns the homogeneous form of the boundary conditions
        """

        if self.bcs is None:
            return None
        hbcs = self.bcs(V)
        if hbcs is not None:
            [bc.homogenize() for bc in hbcs]

        return hbcs
    
    def extremenode(self,index,complement = True):
        """
        It returns in order:
        - the first extreme node
        - a node in the interior of the  neighbour
        - the second extreme node
        """
        
        neigh=self.manifold.neighbour[index]
        nodes=self.manifold.extremenode(index,types="index")
        nodes.append(self.manifold.complement(index,[index,nodes[0],nodes[1]]))
        ac_=[]
        for node in nodes:
            ac_.append(Function(self.ac_space))
            self.load_from_index(node,ac_[-1])
        return ac_
    

    def orthonormal(self,vector1,vector2):
        """
        the function receive two vector in the Mixed space 
        and applying Gram-Smith it turns it in a orthonormal basis
        """
        
        norm1 = sqrt(assemble(inner(vector1,vector1)*dx))
        vector1.assign(vector1/norm1)

        alpha = assemble(inner(vector1,vector2)*dx)
        vector2.assign( vector2 - alpha*vector1)
        norm2 = sqrt(assemble(inner(vector2,vector2 )*dx))
        vector2.assign( vector2/norm2)
        
        
    def load_solution(self, z, param, ac_state):
        
        assign(ac_state.sub(0), z)
        r =Function(self.R)
        r.assign(param)
        assign(ac_state.sub(1), r)
        
    def load_from_index(self, node, state, *arg, **kwargs):
        """
        Given the index of the node it updates the state function with the corresponding solution;
        if requested it returns also the tangent vectors
        """

        par= self.manifold["node", node, 0]
        z = Function(self.V, self.prefix + "/point-%d-state.xml" % node)
        self.load_solution(z, Constant(par), state)
        
        if "tangent" in kwargs.keys():
            kwargs["tangent"][0].assign(Function(self.ac_space, self.file+"/db/point-%d-tangent-basis-0.xml" % node))
            kwargs["tangent"][1].assign(Function(self.ac_space, self.file+"/db/point-%d-tangent-basis-1.xml" % node))
     
    def delete_solution(self,check):
        """
        given a list with the index of the point it deletes both the corresponding state
        or in case it exists the file containing the tangent plane
        """
        
        for node in check:
            os.remove(self.prefix + "/point-%d-state.xml" % node)
            
            try:
                os.remove(self.file+"/db/point-%d-tangent-basis-0.xml" % node)
                os.remove(self.file+"/db/point-%d-tangent-basis-1.xml" % node)
            except IOError:
                pass
            
                
    
    def ProjectTangent(self,vec,centre,tangent):
        """  
        Given a vector in the space VxR^2 it returns its projection on the tangent 
        plane at the point centre as a numpy_vector
        """
        
        phi1=tangent[0]
        phi2=tangent[1]
        x1 = assemble(inner(vec-centre,phi1)*dx)
        x2 = assemble(inner(vec-centre,phi2)*dx)
        return np.array([x1,x2])
	
    def gap_angle(self,ac,tangent,index,n_s=None):
        """
        The function receive as an input the centre node and returns a tuple with:
        -gap angle in (0,2*pi)
        - +1 if to fill the gap we move anticlockwise (we need to add the angle to y1), -1 otherwise
        - the angle alpha from which we start filling the gap
        - the shortest edge used as a reference to compute the step
        """

        if n_s is None:
            n_s = self.manifold["node",index,1]
        
        if n_s==0:
                return [2*pi, +1, 0.0, (self.step_max+self.step_min)*0.5]
              
        if n_s==1:# mean value
            (node1,node3) = self.extremenode(index)
            node2 = Function(self.ac_space)
            node2.assign(0.5*(node1+node3))
        else:
            try:
                (node1,node3,node2) = self.extremenode(index)
            except:
                pdb.set_trace()

        
        # we evaluate the tangent plane and save it, after the node has been explored the information can be delated 
        y1 = self.ProjectTangent(node1,ac,tangent);
        norm1 = sqrt(y1[0]**2+y1[1]**2); y1=y1/norm1;
        y2 = self.ProjectTangent(node2,ac,tangent) 
        norm2 = sqrt(y2[0]**2+y2[1]**2); 
        if norm2==0.0:
             pdb.set_trace()
        y2=y2/norm2;
        y3 = self.ProjectTangent(node3,ac,tangent); 
        norm3 = sqrt(y3[0]**2+y3[1]**2); y3=y3/norm3;
        
        alpha = np.arctan2(y1[1],y1[0]) # starting angle in computing the new prediction

        if alpha < 0:
            alpha = 2*pi+alpha

        A = np.array([[y3[0],y3[1]],[-y3[1], y3[0]]]) # clockwise rotation of angle between y3 and e1=[1;0]
        y1 = A.dot(y1)

        y2 = A.dot(y2)

        beta1 = np.arctan2(y1[1],y1[0])

        if beta1<0:
            beta1=2*pi+beta1
        beta2 = np.arctan2(y2[1],y2[0])

        if beta2<0:
            beta2=2*pi+beta2
        if beta1>beta2:
            gamma = 2*pi-beta1
            direction = +1
        else:
            gamma =beta1
            direction = -1
        return [gamma,direction,alpha,max(min(self.step_max/self.tau,norm1,norm3)*self.tau,self.step_min)]
