import pdb
from dolfin import *
from numpy import pi,cos,sin
import numpy as np
from param_mesh import DynamicMesh, FrontalList
from multi_problem import MultiProblem, fetch_R
import os
from newton import newton
"""
	This version is based on the archleght class in defcon
"""

def arclength(problem, worker, max_iter,lam_0, u_0=None, file=None, overlap=False):

    num_branch = worker.branches

    manifold = DynamicMesh(worker.bbox[-1])
    worker.add_manifold(manifold)
    frontal = FrontalList(manifold)
    problem.SetManifold(manifold)

    (function_space, R, W) = problem.SetupSpaces()
    problem.file = file
    problem.prefix= file+"/db %d" % num_branch

    if problem.bcs is not None:
        bcs=problem.bcs(W.sub(0))
    else:
        bcs=None

    success = problem.add_initial_condition(lam_0, u_0, frontal, worker)

    if not success:
        del frontal
        del manifold
        worker.branches -= 1
        del worker.bbox[-1] 
        return

    state = Function(W)
    prev = Function(W)
   
    tangent = [Function(W),Function(W)]

    tangent_prev = [Function(W),Function(W)]

    test = TestFunction(W)
    trial =TrialFunction(W)
    dh = Constant(0)

    direction = [Constant(0), Constant(0)]

    consts = Constant([0.0,0.0])
    ac_residual = problem.ac_residual(state, prev, dh, direction, test, tangent)
    ac_jacobian = problem.ac_jacobian(ac_residual, state, trial)

    hbcs = problem.boundary_conditions(W.sub(0))
    stop = 7000

    while len(frontal.dic)>0:
        # selection of the new element
        (centre,info,idcentre,num_simplex)=frontal.get()

        solution = Function(function_space, problem.prefix + "/point-%d-state.xml" % idcentre)
        consts.assign(Constant(centre))

        problem.load_solution(solution, consts, state)
    
        #Step 1. Compute the a basis for the tangent plane if not already saved
        if "tangent" in info: # 1 if we have already computed the tangent space
            tangent[0].assign(Function(W, file+"/db/point-%d-tangent-basis-0.xml" % idcentre))
            os.remove(file +"/db/point-%d-tangent-basis-0.xml" % idcentre)
            tangent[1].assign(Function(W, file+"/db/point-%d-tangent-basis-1.xml" % idcentre))
            os.remove(file+"/db/point-%d-tangent-basis-1.xml" % idcentre)
        else:
            success=problem.base_tangent_plane(state, tangent, test, trial, None, hbcs)
            if not success:
                print("No tangent plane for node %d"%idcentre)
                continue

        if "gap_angle" in info:
            (gap_angle,sign,alpha,step) = info["gap_angle"]
        else:
	    try:
            	(gap_angle,sign,alpha,step) = problem.gap_angle(state, tangent, idcentre, n_s=num_simplex)
	    except:
		print("No gap angle,id centre=%d"%idcentre)
		continue

        # Step 2. Update the state guess with the tangent plane
        prev.assign(state)

        dh.assign(step)
        tangent_prev[0].assign(tangent[0])
        tangent_prev[1].assign(tangent[1])
        try:
            n = max(1, int(round(3.*gap_angle/pi)))
        except:
            print("problem in the gap_angle=%d",gap_angle,"for centre %d",idcentre)
            continue
        gamma = float(sign)*gap_angle/n
        

        if n==1:
            check=set()
            if gap_angle < problem.gamma_min:

             # we complete the neighbour of the current centre replacing the extreme nodes with
             # their mid point
                direction[0].assign(Constant(cos(alpha+gamma*0.5))) 
                direction[1].assign(Constant(sin(alpha+gamma*0.5))) 

                while float(dh) >= problem.step_min: 
                    state.assign(prev + float(dh*direction[0])*tangent_prev[0] + float(dh*direction[1])*tangent_prev[1])
                    (success,iteration) = newton(ac_residual, ac_jacobian, state, bcs) 

                    
                    if success & iteration<=max_iter:
                        break

                    dh.assign(float(dh)/problem.tau)
                    success = False
                    
                
                if success:
                    z = problem.ac_to_state(state)
                    lmbda = problem.ac_to_parameter(state)
                    param = fetch_R(lmbda)
                    index,check = manifold.add_node(param, centre=idcentre,task="merge",flag=check)
                    problem.delete_solution(check)
                    
                    func=[fun(z) for fun in problem.functional]
                    manifold.functional_add(index,func)
                    
                    if problem.InOmega(index=index):
                        # we need to add it to the front
                        flag=problem.base_tangent_plane(state, tangent, test, trial, tangent_prev, hbcs)

                        if flag:

                            File(file+"/db/point-%d-tangent-basis-0.xml" % index) << tangent[0]
                            File(file+"/db/point-%d-tangent-basis-1.xml" % index) << tangent[1]
                        
                            if frontal.insert(index,info={"tangent":True}):
                                args={"gap_angle":problem.gap_angle(state,tangent,index)}
                                frontal.update_info(index,**args) 
                        else:
                        	print("No tangent plane")
                         
                    File(problem.prefix + "/point-%d-state.xml" % index) << z
                    del z
                    del lmbda
		    print("merged")
                else:
                    print("Failing to merge")
            else:

                check = manifold.close(idcentre,flag=check) 
                for node in check:
                    problem.load_from_index(node,state,tangent=tangent)
                    karg={"gap_angle":problem.gap_angle(state,tangent,node)}
                    frontal.update_info(node,**karg)	
        else:

            check=set()
            print("centre index=",idcentre,"gap_angle=",gap_angle,"num_simplex=",num_simplex)
            for i in range(1,n):
                # updating the step
                direction[0].assign(Constant(cos(alpha+gamma*i))) 
                direction[1].assign(Constant(sin(alpha+gamma*i)))
                # we use an adaptive loop at each iteration if the solution
                # does not converge we reduce the step

                
                while float(dh) >= problem.step_min:
                    # update the prediction and project it on the manifold
                    state.assign(prev + float(dh*direction[0])*tangent_prev[0] + float(dh*direction[1])*tangent_prev[1])
                    
                    (success,iteration) = newton(ac_residual, ac_jacobian, state, bcs) 
                    
                    
                    if (success) & (iteration<=max_iter):
			if overlap:
			    if not worker.old_branch(state,self_overlap=True,save=False):
                            	break
			else:
			    break
                        
			
                    success = False
                    dh.assign(float(dh)/problem.tau)


                if success:
                    z = problem.ac_to_state(state)
                    lmbda = problem.ac_to_parameter(state)
                    param = fetch_R(lmbda)

                    index,check = manifold.add_node(param, centre=idcentre, flag=check)
                    func=[fun(z) for fun in problem.functional]
                    manifold.functional_add(index,func)
                    
                    print("Inseting node %d with parameter:" %index,param,"and tangent_plane:",tangent_prev[0].vector().get_local(),tangent_prev[1].vector().get_local())
                    distance=sqrt(assemble(inner(state-prev,state-prev)*dx))
                    print("functional=",func,z)
                    print("The step required is %.10f" %float(dh)+ "while the actual distance is %.10f"%float(distance))
                    
                
                    
                    if problem.InOmega(index=index):
                        # we need to add it to the front
                        flag = problem.base_tangent_plane(state, tangent, test, trial, tangent_prev, hbcs)

                        if flag:
                            File(file+"/db/point-%d-tangent-basis-0.xml" % index) << tangent[0]
                            File(file+"/db/point-%d-tangent-basis-1.xml" % index) << tangent[1]

                            if frontal.insert(index,info={"tangent":True}):
                                karg={"gap_angle":problem.gap_angle(state,tangent,index)}
                                frontal.update_info(index,**karg)
                        else:
                        	print("Point with no tangent plane")
    
                    File(problem.prefix + "/point-%d-state.xml" % index) << z
                    del z
                    del lmbda
                else:
                    print("Minimum step reached and Newton did not converged")
                    break
            
            if success: 
                check = manifold.close(idcentre,flag=check)

                for node in check:
                    problem.load_from_index(node, state, tangent = tangent)
                    karg= {"gap_angle":problem.gap_angle(state,tangent,node)}
                    frontal.update_info(node,**karg)	

            elif (gap_angle-(i-1)*pi/3)<pi:
                print("Fail in convegence, gluing points")
                manifold.close(idcentre,flag=check) 
                for node in check:
		    if manifold["node",node,1] != -1: 
                    	#problem.load_from_index(node, state, tangent = tangent)
                    	#karg= {"gap_angle":problem.gap_angle(state,tangent,node)}
                    	#frontal.update_info(node,**karg)
			frontal.delete(node)
                print(len(frontal.dic))
            else:
		print("Failing to converge: centre node %d "%idcentre)



        if (manifold.indexnode >= stop):
            stop+=1000
            mesh=Mesh()
            manifold.create_mesh(mesh,0)
            File("manifold/mesh%d.pvd"%manifold.indexnode) << mesh
	
           # if worker.check_frontal(frontal):
           #     continue
     #   elif manifold.indexnode > stop:
     #       stop += 500
     #       mesh=Mesh()
            #manifold.create_mesh(mesh,0)
            #File("manifold/mesh_%d"%num_branch+"_%d.pvd"%manifold.indexnode) << mesh
    
    if len(manifold.node)>7:
        for i in range(len(problem.functional)):
            mesh=Mesh()
            manifold.create_mesh(mesh,i)
            File(file+"/fun%d" % i + "/branch%d.pvd"%num_branch ) << mesh
            
            del mesh 
            
    else:
        
        del frontal
        del manifold
        worker.branches -= 1
        del worker.bbox[-1] 
        return
    
    #plot(diff,'b*')
    #savefig("difference.pdf")

