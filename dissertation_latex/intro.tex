\chapter{Introduction}

Mathematical models have proven to be effective tools in a wide range of fields including biology, physics, economics and engineering. Their predictive power has entirely reshaped how problems are tackled, giving the power to test and analyse several settings without having to replicate them experimentally. In the particular case of dynamical system models, the basic assumption is that there exists a law uniquely relating the current and past configurations of the system with the ones it assumes in the future. In other words, the evolution of the state follows a precise pattern which can be translated into the language of mathematics using equations. These deterministic representations of real phenomena are just approximations, partial yet useful descriptions of reality. Given the model, a wide range of mathematical techniques are then available to give insights to many properties of the system. In particular, linear systems have been researched extensively. Since they can be solved exactly, their analysis is easier when compared with non-linear dynamics. However, the corresponding description of reality is so simplified that it may not capture some of the essential characteristics seen in reality. For this reason, the past few decades have witnessed a shift of attention toward non-linear dynamical systems, which is also the focus of this work. More precisely, we are interested in their behaviour over long-time scale. 
Given a general set of variables $u$, which can be a scalar or a vector, the general form for these systems is:

\begin{equation}
\begin{aligned}
	&\frac{\partial\, u}{\partial\, t} = f\left(u,\lambda\right)\\
	&u= u_0 
\end{aligned}	
	\label{eq0}
\end{equation} 

where $f: U \times P \rightarrow V$ is a generic non-linear operator, $t$ is time, $u_0$ is the initial condition and $\lambda$ is the set of problem parameters. For this reason, $P$ is called \textit{parameter space} and it refers to a subset of $\mathbb{R}^n$, where $n$ represents the number of parameters in the problem. Unlike variables, parameters can not be directly measured in an experiment. They can either be associated with inherent properties of the system, such as the resistance of a conductive material, or with the experiment set-up, such as the intensity of the current the material is subjected to. In this second case, we use the specific term \textit{control parameters}. If $f$ determines the general dynamics followed by a phenomenon, the parameters specify its key features. Consequently, depending on the context one or more parameters might be necessary to fully adjust the prescribed model to the specific system analysed. 

Bifurcation theory is the area of mathematics specifically devoted to studying the influence of parameters on the existence of potential behaviours of a dynamical system. More precisely, it focuses on the identification and classification of \textit{stationary points}, while smoothly varying one or more parameters. This is particularly interesting in the case of non-linear dynamics. Unlike the linear case, their \textit{state space} can be characterised by multiple attracting sets. Consequently, the asymptotic behaviour of the system is strictly dependent on its initial condition. Apart from common fixed points, this might converge also to limits circles or \textit{strange attractors}, typically associated with \textit{chaos}. The examples we will present are confined to the computation \textit{equilibria} of~(\ref{eq0}), i.e. $f(u,\lambda)=0$. However we can encompass all the different cases with the general equation:

\begin{equation}
	F\left(u,\lambda\right)=0. 
	\label{eq1}
\end{equation} 
where $F: U \times P \rightarrow V$ is a again a non-linear operator usually obtained from $f$.
Given the pair $(u, \lambda)$ for which Equation~(\ref{eq1}) holds, our intuition suggests that small perturbations in the parameter values will result in a gradual and small change in the behaviour of the system. In other words, we expect that there exists a continuous function $u(\lambda)$, such that $	F\left(u(\lambda),\lambda\right)=0$. This familiar idea is mathematically formalized in the \textbf{Implicit Function Theorem}, \cite[Theorem I.1.1]{bif:1}. However, this is just a local results that holds only under specific assumptions placed on the operator $F$. When these are not satisfied, we are in the presence of \textit{bifurcations}, which we will also call \textit{critical point}. These corresponds to parameter values for which the system undergoes a transition between two qualitatively different states. Apart from this theoretical prospective, this results is essential to the understanding of natural phenomena that have puzzled the scientific community over centuries, such as the appearance of Taylor vortices and the buckling of the Euler beam \cite{bif:5}. The latter plays a central role in the study of structural analysis, since it accounts for the deformation of beams under external loads. In order to have an overview of the qualitative behaviour of the system we refer to \cite{Caspthesis}, where an ideal rod is used as a reference. However real objects do present imperfections, which can also be seen as a perturbation of the underlying model. Let us now suppose we apply the same weight $\lambda$ to both a perfect and an imperfect beam. Their response is illustrated in Figure \ref{fig1}. Due to the small imperfection $mu$, the symmetry of the initial idealized model is broken. Consequently, this more realistic beam deflects continuously as the load is increased, until it breaks. This behaviour is qualitatively different from the one shown by the idealized beam. This remains initially straight, until a sudden transition that occurs for a critical value of the load $\lambda_c$. Due to the symmetric nature of the problem, the beam is equally likely to deflect either to the left or to the right. In Figure \ref{fig1c} we have included both options, highlighted by different colours. While in continuous mechanics this behaviour is called buckling, in mathematical terms, this correspond to a \textit{pitchfork} bifurcation, see \cite{bif:1}.

\begin{figure}[h!]
	\begin{subfigure}[l]{0.3\textwidth}
		\def\svgwidth{2.5\columnwidth}
		\resizebox{1.3\textwidth}{!}{\input{undeformed.pdf_tex}}
		\caption{}
	\end{subfigure}
	\begin{subfigure}[c]{0.3\textwidth}
		\def\svgwidth{2.5\columnwidth}
		\resizebox{1.3\textwidth}{!}{\input{deformed.pdf_tex}}
		\caption{$\lambda<\lambda_{c}$}
	\end{subfigure}
	\begin{subfigure}[r]{0.3\textwidth}
		\def\svgwidth{2.5\columnwidth}
		\resizebox{1.3\textwidth}{!}{\input{final_deformation.pdf_tex}}
		\caption{$\lambda\geq\lambda_{c}$}
		\label{fig1c}
	\end{subfigure}
\caption{Deformation of an ideal $($left$)$ and an imperfect $($right$)$ beam under a load $\lambda$. While the second beam qualitatively changes as soon as a load is applied, the first one present relevant deviation from the vertical state only for weights greater than a critical value, $\lambda_c$, called \textit{Euler's critical load}, \cite{bif:5}. Note that the  segment $\mu$ is not in scale. This is assume it to be negligible with respect to the length of the beam.}
\label{fig1}
\end{figure}

While in the idealised setting a complete description of the system requires only the knowledge of the load applied. The configuration assumed by the deformed beam is also sensitive to the value of $\mu$. Indeed, due to this imperfection, the load generates both a transverse and an axial force, which is absent in the ideal case. Consequently, this situation can not be depicted by the reduced model containing only one parameter. From this point of view, pitchfork bifurcations are just artificial; a limitation of the model capabilities, which requires the introduction of easily-breakable symmetries.  

This is just one example among several cases in which single-parameter models are inadequate. Let us consider the phenomenon of \textit{hysteresis}, which accounts for the dependence of the state of the system on its history. Commonly encountered in physical application, such as in the study of magnets, this is characterized by the presence of two or more \textit{turning} points in the state space. While this naturally occurs in two-parameter models, there is no analogue in the study of bifurcation of system with a single parameter.
 
Despite the relevant role played by multi-parameter models, their additional complexity has caused a profound gap between the research in this direction and the advanced techniques developed for the one-parameter case. Our study falls exactly in between, as an attempt to narrow this discrepancy. 

\begin{figure}
	\vspace{-5mm}
	\centering
	\begin{subfigure}{0.48\textwidth}
		\centering
		\resizebox{0.8\textwidth}{!}{\input{surface.pdf_tex}}
		\vspace{-20mm}
		\caption{Connected branches}
		\label{fig2a}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\centering
		\resizebox{0.75\textwidth}{!}{\input{surface2.pdf_tex}}
		\vspace{-20mm}
		\caption{Disconnected branches}
		\label{fig2b}
	\end{subfigure}
	\caption{Example of bifurcation diagrams. Different branches are highlighted with different colours, while the red curve consist of bifurcation points.}
	\label{fig2}
\end{figure}

As mentioned above, our aim is to study the qualitative asymptotic behaviour of a system as the parameter values vary. An effective way to visualize this relies on the \textit{bifurcation diagram}, which illustrates how the solution $u$ changes in the parameter space. In the particular case of two parameters, the solutions $\left(u,\lambda\right)$ of Equation~(\ref{eq1}) define a manifold of dimension 2 in the space $V\times\mathbb{R}^2$. In other words a surface embedded in a space of either finite or infinite dimensional space. Due to the limited number of dimensions we are able to perceive, unless $dim(V)=1$, we can not directly visualize it. For this reason, we need to project the actual manifold onto a 3 dimensional space. This can easily be done plotting a functional $J$ of the solution, $J: V \rightarrow \mathbb{R}$, as shown in Figure \ref{fig2}. Since this might be a source of confusion later, it is important to clarify the dimensional reduction is applied just for visual reason. As mentioned above, our objective is computing the original manifold, which we will denote by $M$. The latter consists of smooth surfaces called branches. These can either intersect at bifurcation curves, as in Figure \ref{fig2a}, or be disconnected, as in Figure \ref{fig2b}.  

Due to the non-linear nature of the problem, Equation~(\ref{eq1}) can not be solved analytically. However, a combination of theoretical knowledge and modern computational power gives us the resources necessary to overcome this limit. In order to fully depict our bifurcation diagram, we need to combine different techniques. Given a good initial guess, we know how to estimate a point $Q$ on $M$ by solving Equation~(\ref{eq1}) with \textit{Newton's method}. 

\textit{Multi-parameter continuation} allows us to compute the whole branch connecting the points in a clever way. Different algorithm have been proposed to generalised the \textit{arc-length} technique,\cite{bif:6}, adopted in the 1D case. This task is not as trivial as it might seem and it is the step where most of the additional complexity comes in, when moving to higher dimension continuation. Different examples have been proposed in the literature but mainly applied to algebraic problems. Less attention has been paid to the case in which $F$ is a differential operator, due to the relevant computational power and memory it requires. The few studies proposed in the literature usually rely on the techniques available for single parameter analysis, as in the work of Chang et al., \cite{bif:2}. The overall behaviour is extrapolated by information collected along several slices of the surface. In other words, fixing one of the two parameter, the corresponding bifurcation curves are computed starting from a given initial guess. However, the result is a partial understanding of the behaviour which can miss key information in the presence of singular points. In order to obtain more rigorous result, a multi-parameter continuation technique in infinite dimension needs to be consider. From this perspective, the recent work by Gaimero et Al. in \cite{bif:4} is more promising. Here, the simplicial technique, which was first proposed by Brodzik et al. in \cite{bif:7}, is adapted to compute the equilibria manifold for the Cahn-Hilliard equation. Using a similar approach in the local approximation of the surface, several adjustment have been introduced in order to make it suitable for our purposes.
   
However, the main challenge is the global construction of the manifold, which requires the assembling of the different branches. The most common approach relies on a direct detection of bifurcations based on the \textit{branch-switching} method. Inherited by the one-parameter literature, this allows us to move from one branch of the manifold to another. Extended by Henderson to multi-parameter continuation, \cite{bif:8}, the author has included branch-switching to the continuation software \textit{Multifario}. Successfully tested for ODEs systems and a limited class of boundary value problems, this can not be efficiently scaled to higher dimensional problems. The limitations of this method are already evident in the single-parameter case. As discussed in \cite{Caspthesis}, apart from the relevant computational cost, its main constrain is that it can only compute branches connect to the initial point given. For this reason, the past few years have witnessed an increasing interest in alternative techniques. Particularly successful has been the use of \textit{deflation}. First proposed by Wilkinson to compute the roots of polynomials, this technique has been recently adapted by Farrell et Al., \cite{bif:9}, to the general case of finding solutions to~(\ref{eq1}). Even though, this study already includes the multi-parameter case, combining deflation and multi-continuation in practice is not as natural. For this reason, attention has only been paid to the one-parameter case, where the power of this novel approach has been demonstrate. Relying on the \textit{defcon} library, the bifurcation diagram of large-scale systems and unknown disconnected branches have been explored for the first time,\cite{bif:10}. Inspired by these results, we have enhanced current two-parameter simplicial continuation with this novel approach. The resulting algorithm has the potential to explore unknown branches, to give more complete insights into the complex behaviour of dynamical systems. 

\section{Outline of the Work}

Having specified the main goal of this thesis, we have also outlined its potential applications in the study of dynamical system. However, despite this specific aim, continuation techniques apply to a wider class of problems. Indeed, Equation~(\ref{eq1}) does define the steady-state solutions of (\ref{eq0}), but it also implicitly determines a geometrical object in the product space $V \times P$. In particular, under proper but weak condition on the operator $F$ in~(\ref{eq1}), this expression defines a \textit{Manifold}. Continuation techniques have been designed to compute its numerical approximation. In order to properly manipulate this tool, we will first review some key properties of these structures, that originally belongs to the realm of \textit{topology}. This will give us the required knowledge to discuss the current state of the art of multi-parameter continuation. After this general overview, only two approaches will be further analysed: the one proposed by Gaimero et Al. in \cite{bif:4} and  the one by Henderson in \cite{bif:11}. From this direct comparison, the first one will emerge as the algorithm that better matches our needs. 

However, as already mentioned in the introduction, we have introduced several adjustments. These do not just concern its actual implementation but also the framework in which the method is developed. Unlike previous works in the literature, we introduce the algorithm with the formalism proper of Banach spaces and variational calculus. This integrates perfectly with the interface of FEniCS, the python software for solving PDEs our code is built on. 

After having looked in detail at the continuation of smooth branches, we move to the global assembling of the manifold. This is where the novel introduction of deflation will play a key role. We will start from a brief summary of its convergence results, easily adapted to the multi-parameter case. We will then pay more attention to how this can be numerically combined with the continuation algorithm described in the previous chapter. to give a complete picture of the bifurcation diagram of a system.

Finally, the last section of this work will be devoted to test our method. Several examples are proposed. We will start from the analysis of ODEs system, where Multifario software can be used as a benchmark. After that, we will look at more complex system, which could not be previously tackled.