\chapter{Continuation Techniques}

As outline in the previous chapter, the first part of this work will be devoted to an overview of the current state of art of multi-parameter continuations techniques. Even thought we will applied it to compute the bifurcation analysis of dynamical system, their application exceed our specific aim. In order to understand the foundation of these techniques, we first need to review some basic concepts of \textit{topology}. However, before looking in details at the connection with this other branch of mathematics, we remind the reader some notation we will use throughout this first chapter. Looking back at the definition of~(\ref{eq1}), we consider a general non-linear operator $F$ on the function space $W$, where $W=V\times P$ and $P$ is equal to $R^n$. This allows us to extend our current discussion to the case of a general number of parameters $n$, with $n\geq2$. Due to the several simplification that can be applied to the case $n=1$, this has always been considered separately. Following this common practise, we will refer to this special case as single-parameter continuation to underline the profound difference not in the nature of problem, but in how this is faced. 

As highlight in the introduction, our aim is to find all the solutions of~(\ref{eq1}). Due to the non-linearity in the operator $F$, these can not usually be computed analytically. Thus, we need to rely on a the power of computing to obtain an accurate but not exact solution.
This same problem can be reformulated in the more abstract setting of \textit{topology}. More precisely, we will rely on the concept of \textit{Banach manifolds}. Since this is just a useful tool to reach our goal, the discussion of this topic will be limited to those aspects that will be useful in development of our algorithm. This do not require to have any previous acquaintance with the material. For the reader who is interested in investigating it further, we refer to \cite{bif:3}.

Starting from Equation~(\ref{eq1}), we defined the following mathematical object:

\begin{equation}
\label{eq2}
 M=\left\{w\in W\left|\right. F(w) = 0\right\}.
\end{equation}

Let $F$ be a functional of class $\mathcal{C}^k$, with $k\geq2$. Then, $M$ is an implicitly defined manifold of dimension $n$ embedded in the Banach space $W$. Hence the name \textit{Banach manifold}. According to the space in which it is defined, there are other types of infinite-dimensional manifold, such as \textit{Frèchet} ot \textit{Hilbert} manifolds. Given this framework, we can translate our original problem in finding a numerical approximation of $M$. However, before doing that, we need to better understand its nature and characteristics. 

\section{Banach Manifold}
Let start our brief discussion of Banach Manifolds from a less formal definition of these objects. 

\begin{definition}
	A Banach Manifold is a topological space that locally resembles a Banach Space. In other words, for each element $Q$, $Q \in M$, there exist a neighbourhood $U$ of $Q$, which is homeomorphic to an open set in a Banach Space.  
\end{definition}

Apart from some small technicalities, there is a key aspect we want the reader to keep in mind. Let us use an analogy. We are given a box of Lego and a set of instruction. The final goal is construct a complex object starting by this building box. Even though we do not know what this object is, we are given a list of the piece we need; each component might be described by its size and colour. We are also given a set of rules on how the different pieces needs to be assemble. This is in principle sufficient to reach our goal without any information on the global structure of the complete object. In the same way, we can build a manifold starting from its elementary component, the neighbours, and a map, the homeomorphism, that relates them. 

Having this image in mind, we now move to translate it in the formalism proper of mathematics. In doing so, we start from the concept of \textit{atlas}, which corresponds to the set of instruction according to our analogy. Even though this notion can be applied to a general set, in order to avoid confusion, we will adapt the definition given in \cite{bif:3} to the particular case analysed.

\begin{definition}
	Given the set $M$, an \textbf{atlas of class} $\mathcal{C}^p$, with $p\geq0$ on $M$ is a collection $\left\{\left(U_i,\varphi_i\right)\left|\right. i \in \mathcal{I} \right\}$, indexed by the set $\mathcal{I}$ satisfying:
	\begin{itemize}
		\item[$\left(i\right)$] Each $U_i$ is a subset of $W$ and $\bigcup\limits_{i \in \mathcal{I}} U_i = W$;
		\item[$\left(ii\right)$] Each $\varphi_i$ is a \textit{bijection} of $U_i$ onto an open set $\phi_i U_i$ of a Banach space $\mathbb{E}$ such that for any $i,j \in \mathcal{I}$, $\varphi_i(U_i\cap U_j)$ is open in $\mathbf{E}$;
		\item[$\left(iii\right)$] the map
		\begin{equation*}
		\varphi_j\varphi_i^{-1}: \varphi_i\left(U_i\cap U_j\right) \rightarrow \varphi_j\left(U_i\cap U_j\right)
		\end{equation*}
		is a $\mathcal{C}^p-$isomorphism for each pair of indices i,j.
		\end{itemize}
	Each pair $\left(U_i,\varphi_i\right)$ is called \textbf{chart}. 
	\label{atlas}
\end{definition}

We also introduce the notion of \textit{compatibility} between a chart and a atlas.

\begin{definition}
	Let us consider a chart $(U,\varphi)$, where U is an open subset of $M$ and an isomorphism $\varphi: U\rightarrow U'$, with $U'$ subset of $\mathbf{E}$. Then this is said to be \textbf{compatible} with $\left\{(U_i,\phi_i)\right\}$ if
	
	$\forall\  i \in \mathcal{I}, \  \varphi_i\varphi^{-1}$ satisfies condition $\left(iii\right)$ in Definition \ref{atlas}
	
\end{definition}  

Given the set of atlases, we can defined an equivalence relation based on the idea of compatibility. More precisely two atlases are equivalent if each chart of one is compatible with the other atlas. This allow us to define the equivalence class of $\mathcal{C}^p$-atlases on $M$ which is said to define a $\mathcal{C}^p$-manifold on $M$. This is equivalent to say that $M$ is a manifold modeled on $\mathbf{E}$. 

Again, following the analogy above, we can have different procedures which led to assemble the same structure. If each of them uniquely define the object, the other implication does not hold. Since there is no privileged set of instruction, the object itself identifies the entire class of instructions, which allow to build it. Similarly, the class defines the object. Hence, the definition above.

Looking back at~(\ref{eq2}), the space $\mathbf{E}$ is equal to $R^n$. Consequently the isomorphism $\varphi_i$ maps a neighbourhood of $M$ to the open unit ball in $R^n$. Using the notation $\mathcal{B}_1$, we have that $\mathcal{B}_1=\left\{x \in R^n , \left|x\right|<1\right\}$. 
  